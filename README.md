# i3-presentation-mode

The easiest way to automatically lock your screen using the window manager i3 is to use i3-lock and xautolock. Of course there are occasions when you want keep your screen from locking. This simple script does exactly that.

## Installation
1.  place `i3-presentation-mode.sh` somewhere (e.g. /usr/bin if you want it to be accessible by any user) and make it executable.
2.  add shortcuts by adding the following lines to your i3 config file, typically located at `~/.config/i3/config`:
    ```
    #i3-presentation-mode shortcuts
    bindsym $mod+Shift+p exec i3-presentation-mode toggle
    ```
3.  add notification to i3-status bar
    *  Copy wrapper.py somewhere (unless you are already using some sort of wrapper for i3status - if you do, you will have to do the next step according to your wrapper)
    *  Change 
    ```
    bar {
        status_command i3status
	    #[...]
    }
    ```
    to
    ```
    bar {
        status_command i3status | python3 [location of the wrapper]
	    #[...]
    }
    ```
    
## Usage
Apart from the shortcuts, i3-presentation-mode is a bash script with the following command line options
*  `i3-presenation-mode.sh` - Returns **Presentation mode: On** if presentation mode is on, nothing if it is not.
    > In fact, this returns *RUNNING_MSG* if *LOCKER_CMD*(defaulted to empty string) is running and *STOPPED_MSG*(defaulted as **Presentation mode: On**) if it is stopped.
*  `i3-presenation-mode.sh toggle` - Toggles presentation mode
*  `i3-presenation-mode.sh enable` - Turns presentation mode on (turns screensaver off)
    > In fact this stops the process *LOCKER_CMD*, that can be changed in `i3-presenation-mode.sh`
*  `i3-presenation-mode.sh disable` - Turns presentation mode off (turns screensaver on)
    > In fact this continues the process *LOCKER_CMD*